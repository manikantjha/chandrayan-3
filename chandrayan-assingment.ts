type Direction = 'N' | 'S' | 'E' | 'W' | 'Up' | 'Down';

interface Coordinates {
    x: number;
    y: number;
    z: number;
}

class Spacecraft {
    coordinates: Coordinates;
    direction: Direction;

    constructor(x: number, y: number, z: number, direction: Direction) {
        this.coordinates = { x, y, z };
        this.direction = direction;
    }

    moveForward(): void {
        switch (this.direction) {
            case 'N':
                this.coordinates.y += 1;
                break;
            case 'S':
                this.coordinates.y -= 1;
                break;
            case 'E':
                this.coordinates.x += 1;
                break;
            case 'W':
                this.coordinates.x -= 1;
                break;
            case 'Up':
                this.coordinates.z += 1;
                break;
            case 'Down':
                this.coordinates.z -= 1;
                break;
        }
    }

    moveBackward(): void {
        switch (this.direction) {
            case 'N':
                this.coordinates.y -= 1;
                break;
            case 'S':
                this.coordinates.y += 1;
                break;
            case 'E':
                this.coordinates.x -= 1;
                break;
            case 'W':
                this.coordinates.x += 1;
                break;
            case 'Up':
                this.coordinates.z -= 1;
                break;
            case 'Down':
                this.coordinates.z += 1;
                break;
        }
    }

    turnLeft(): void {
        switch (this.direction) {
            case 'N':
                this.direction = 'W';
                break;
            case 'S':
                this.direction = 'E';
                break;
            case 'E':
                this.direction = 'N';
                break;
            case 'W':
                this.direction = 'S';
                break;
        }
    }

    turnRight(): void {
        switch (this.direction) {
            case 'N':
                this.direction = 'E';
                break;
            case 'S':
                this.direction = 'W';
                break;
            case 'E':
                this.direction = 'S';
                break;
            case 'W':
                this.direction = 'N';
                break;
        }
    }

    turnUp(): void {
        if (this.direction !== 'Down') {
            this.direction = 'Up';
        }
    }

    turnDown(): void {
        if (this.direction !== 'Up') {
            this.direction = 'Down';
        }
    }
}

function processCommand(spacecraft: Spacecraft, command: string) {
    let commandInLowerCase = command.toLowerCase();
    switch (commandInLowerCase) {
        case 'f':
            spacecraft.moveForward();
            break;
        case 'b':
            spacecraft.moveBackward();
            break;
        case 'l':
            spacecraft.turnLeft();
            break;
        case 'r':
            spacecraft.turnRight();
            break;
        case 'u':
            spacecraft.turnUp();
            break;
        case 'd':
            spacecraft.turnDown();
            break;
        default:
            console.log('Invalid command!');
            break;
    }

    console.log('Current position:', spacecraft.coordinates);
    console.log('Current direction:', spacecraft.direction);
}


function run(spacecraft: Spacecraft, command:string) {
     processCommand(spacecraft, command)
}


// Example usage:
const spacecraft = new Spacecraft(0, 0, 0, 'N');
run(spacecraft, 'f'); // Current position: { x: 0, y: 1, z: 0 }
